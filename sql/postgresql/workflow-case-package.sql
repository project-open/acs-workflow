--
-- acs-workflow/sql/workflow-case-package.sql
--
-- Creates the PL/SQL package that provides the API for interacting
-- with a workflow case.
--
-- @author Lars Pind (lars@pinds.com)
--
-- @creation-date 2000-05-18
--
-- @cvs-id $Id: workflow-case-package.sql,v 1.19 2001/11/19 18:20:33 neophytosd Exp $
--

\i workflow-case-package-head.sql
\i workflow-case-package-body.sql