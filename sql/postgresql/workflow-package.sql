 --
-- acs-workflow/sql/workflow-package.sql
--
-- Creates the PL/SQL package that provides the API for defining and dropping
-- workflow cases.
--
-- @author Lars Pind (lars@pinds.com)
--
-- @creation-date 2000-05-18
--
-- @cvs-id $Id: workflow-package.sql,v 1.9 2001/11/19 18:20:33 neophytosd Exp $
--

\i workflow-package-head.sql
\i workflow-package-body.sql

