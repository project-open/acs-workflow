--
-- acs-workflow/sql/wf-callback-package.sql
--
-- Creates the PL/SQL package that provides a small library of reusable
-- workflow callback functions/procedures.
--
-- @author Lars Pind (lars@pinds.com)
--
-- @creation-date 2000-05-18
--
-- @cvs-id $Id: wf-callback-package.sql,v 1.5 2001/11/19 18:20:33 neophytosd Exp $
--

\i wf-callback-package-head.sql
\i wf-callback-package-body.sql
