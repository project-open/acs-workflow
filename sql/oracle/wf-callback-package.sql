--
-- acs-workflow/sql/wf-callback-package.sql
--
-- Creates the PL/SQL package that provides a small library of reusable
-- workflow callback functions/procedures.
--
-- @author Lars Pind (lars@pinds.com)
--
-- @creation-date 2000-05-18
--
-- @cvs-id $Id: wf-callback-package.sql,v 1.2 2001/11/19 18:17:46 neophytosd Exp $
--

@@wf-callback-package-head
@@wf-callback-package-body

