ad_page_contract {
    Move role up.
    
    @author Lars Pind (lars@pinds.com)
    @creation-date Feb 27, 2001
    @cvs-id $Id: role-move-up.tcl,v 1.2 2001/11/19 18:29:29 neophytosd Exp $
} {
    workflow_key:notnull
    role_key:notnull
    {return_url "workflow?[export_vars -url {workflow_key {tab roles}}]"}
}

wf_move_role_up \
	-workflow_key $workflow_key \
	-role_key $role_key

ad_returnredirect $return_url

